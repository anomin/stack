EXECUTABLE=main.hex
SOURCES=main.c list.c msg.c routing.c
TARGET=atmega128rfa1

all: $(EXECUTABLE)

main.elf: $(SOURCES)	
	avr-gcc -mmcu=$(TARGET) -Wall  -o $@ $^ -Os
main.hex: main.elf
	avr-objcopy  -j .text -j .data -O ihex  main.elf main.hex
	avr-size main.elf
 
clean:
	rm -rf main.elf main.hex
program:
	avrdude -c avrispmkII -p m128rfa1 -P usb -U flash:w:main.hex