#ifndef LIST_H
#define LIST_H

#define BROADCAST 0xFFFF

typedef struct {
	uint8_t destinationAddress;
	uint8_t nextHop;
	uint8_t hopCount;	
} routingEntry_t;

typedef struct routingTableNode{
	routingEntry_t entry;
	struct routingTableNode *nextNode;
} routingTableNode_t;



routingTableNode_t *initRoutingTable(routingEntry_t value);
routingTableNode_t *addRoutingEntry(routingEntry_t value, bool add_to_end);
routingTableNode_t *searchRoutingEntry(routingEntry_t value, routingTableNode_t **prev);
routingTableNode_t *deleteRoutingEntry(routingEntry_t value);
//void print_entry_list(void);

#endif /* LIST_H */