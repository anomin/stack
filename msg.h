#define OK 0
#define ERROR 1

typedef struct{
uint8_t *buffer;
bool toSend;
bool toRecv;
} msg_t;

typedef struct {
  msg_t *contents;
  int maxSize;
  int top;
} stack_t;

void initStack(stack_t *stack, int stackSize);
void pushStack(stack_t *stack, msg_t message);
msg_t popStack(stack_t *stack);
int isEmptyStack(stack_t *stack);
int isFullStack(stack_t *stack);




///////////////old api////////////////////
enum msgType { toWrite, toRead, other };
int push(uint8_t *value,uint8_t mask);
msg_t pop(void);
msg_t top(void);