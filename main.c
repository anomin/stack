#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL
#include <util/delay.h>
#include <stdio.h>
#include "main.h"
#include "routing.h"

 
volatile uint8_t rxPayload;
 
uint8_t node_address __attribute__((section(".data"))) = 2;
 


static int write_char(char c, FILE *stream);
int i=0;
uint8_t buffer[128];
 
static FILE my_stdout = FDEV_SETUP_STREAM(write_char, NULL, _FDEV_SETUP_WRITE);

 static int
    write_char(char c, FILE *stream)
    {

      if (c == '\n')
        write_char('\r', stream);
      loop_until_bit_is_set(UCSR0A, UDRE0);
      UDR0 = c;
      return 0;
    }

void USARTInit()
{

UCSR0A = 0;
UCSR0B = _BV(RXEN0) | _BV(TXEN0); //enable RX si TX
UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); //8 data bit
 
UBRR0= 103;

}


 
void initLeds()
{
	DDRB = 0xff;
	PORTB = 0x00;
}
 
void setState(uint8_t state)
{
	TRX_STATE = CMD_FORCE_TRX_OFF;
	TRX_STATE = state;
	while (state != TRX_STATUS_struct.trx_status); 
}
 
// send a short frame with a magic payload
/* void sendFrame()
{
	setState(CMD_PLL_ON);
 
	TRX_FRAME_BUFFER(0) = 4;	//length - minimum length is 3
	TRX_FRAME_BUFFER(1) = 0x06;	//magic payload
	TRX_FRAME_BUFFER(2) = 0x07;
 
	// start transmission
	TRX_STATE = CMD_TX_START;
} */
 
 
// upon receipt make a simple verification of correctness
ISR(TRX24_RX_END_vect)
{
	buffer[0]=TST_RX_LENGTH;
	//printf("%d\n",buffer[0]);
	for (i=1;i<=TST_RX_LENGTH-2;i++)
	{
		buffer[i]=TRX_FRAME_BUFFER(i-1);
		//printf("i:%d-%d\n",i,buffer[i]);
	}

	setState(CMD_RX_ON);
}
 
void rfInit(void)
{
	setState(CMD_TRX_OFF);
 
	IRQ_STATUS = 0xff;
	IRQ_MASK_struct.rx_end_en = 1;
	sei();
 
	//PHY_CC_CCA_struct.channel = 0x0b;
	TRX_CTRL_2_struct.rx_safe_mode = 1;
}
 
int main()
{
	stdout = &my_stdout;
	rfInit();
	initLeds();
	USARTInit();
	setState(CMD_RX_ON);
	initRouting();
	
	while(1)
	{
		if (node_address == 1)
		{
			sendRouting();
			processRouting();
			
			PORTB ^= (1<<PB7);
			_delay_ms(3000);
		}
		else 
		{
			PORTB &= ~(_BV(PB5));
			
			//printf("\n");
			//while (rxPayload != 0) asm("nop");
		}
	}
 
	return 0;
}
