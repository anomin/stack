#include <avr/io.h>
#include <stddef.h> //for NULL
#include <stdbool.h> //for bool
#include <stdlib.h> 
#include "list.h"
#include <stdio.h>




//enum node_t={ self, lider,  }
/*
http://www.cs.jhu.edu/~cs647/dsdv.pdf
http://www.thegeekstuff.com/2012/08/c-linked-list-example/


*/

routingTableNode_t *firstEntry=NULL;
routingTableNode_t *currentEntry=NULL;


routingTableNode_t *initRoutingTable(routingEntry_t value)
{
	
	routingTableNode_t *ptr = (routingTableNode_t*)malloc(sizeof(routingTableNode_t));
	if(NULL == ptr)
        return NULL;

    ptr->entry = value;
    ptr->nextNode = NULL;

    firstEntry = currentEntry = ptr;
    return ptr;
}

routingTableNode_t *addRoutingEntry(routingEntry_t value, bool add_to_end)
{
		
	routingTableNode_t *ptr = (routingTableNode_t*)malloc(sizeof(routingTableNode_t));
	
	if(NULL == ptr)
          return NULL;
    ptr->entry = value;
    ptr->nextNode = NULL;
	
	if(add_to_end)
    {
		currentEntry->nextNode=ptr;
		currentEntry=ptr;	
    }
    else
    {
	
	   ptr->nextNode = firstEntry;
	   firstEntry = ptr;
	 
    }
    return ptr;
	
}
bool isEqual(routingEntry_t first, routingEntry_t second)
{
	if (first.destinationAddress==second.destinationAddress && \
		first.nextHop == second.nextHop && \
		first.hopCount == second.hopCount)
			return true;
	
	return false;
}

routingTableNode_t *searchRoutingEntry(routingEntry_t value,  routingTableNode_t **prev)
{
	
	routingTableNode_t *ptr = firstEntry;
    routingTableNode_t *tmp = NULL;
	
    bool found = false;

		while(ptr != NULL)
    {
        if(isEqual(ptr->entry,value))
        {
            found = true;
            break;
        }
        else
        {
            tmp = ptr;
            ptr = ptr->nextNode;
        }
    }

    if(true == found)
    {
        if(prev)
            *prev = tmp;
        return ptr;
    }
    else
    {
        return NULL;
    }
}
	
	

routingTableNode_t *deleteRoutingEntry(routingEntry_t value)
{
	routingTableNode_t *prev = NULL;
	routingTableNode_t *del = NULL;
	
	//printf("\n Sterg val [%d] din lista\n", value.destinationAddress);
	//printf("\n Sterg val [%d] din lista\n", value.hopCount);
	//printf("\n Sterg val [%d] din lista\n", value.nextHop);
	
	del = searchRoutingEntry(value,&prev);
	
	if(del == NULL)
        return 0;
	else
	{
		if(prev !=NULL)
			prev->nextNode = del->nextNode;
		if(del == currentEntry)
				currentEntry = prev;
			else if(del == firstEntry)
				firstEntry= del->nextNode;
	}
				
	
	free(del);
	del = NULL;
	return 0;

}
/*void print_entry_list(void)
{		
	routingTableNode_t *ptr = firstEntry;

    printf("\n -------Printing list Start------- \n");
    while(ptr != NULL)
    {
		printf("\n [%d] \n",ptr->entry.destinationAddress);
		printf("\n [%d] \n",ptr->entry.hopCount);
		printf("\n [%d] \n",ptr->entry.nextHop);
        ptr = ptr->nextNode;
    }
    printf("\n -------Printing list End------- \n");

    return;
	}
*/


