#include <avr/io.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>

#include "msg.h"

msg_t myMessage;


void initStack(stack_t *stack, int stackSize)
{
	msg_t *newContents;
	newContents=(msg_t*)malloc(sizeof(msg_t) * stackSize);
	if (newContents == NULL);
		// not enoough memory
	stack->contents=newContents;
	stack->maxSize=stackSize;
	stack->top=-1;
}

void pushStack(stack_t *stack, msg_t message)
{
	if (isFullStack(stack));
		//stack is full, alert or something
	stack->contents[++stack->top] = message;
}

msg_t popStack(stack_t *stack)
{
	if (isEmptyStack(stack));
		//return ERROR;
	return stack->contents[stack->top--];
}


int isEmptyStack(stack_t *stack)
{
	return stack->top < 0;
}

int isFullStack(stack_t *stack)
{
	return stack->top >= stack->maxSize - 1;
}

///////////////////////old api//////////////////////////////

int push(uint8_t *value,uint8_t mask)
{
	int i=0;
	uint8_t buffer[128];
	for(i=0;i<value[0];i++)
		buffer[i]=value[i];
	myMessage.buffer=buffer;
	myMessage.toSend=true;
	return 0;
}

msg_t pop(void)
{
	return myMessage;
}

msg_t top(void)
{
	return myMessage;
}