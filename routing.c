#include <avr/io.h>
#include <stddef.h> //for NULL
#include <stdbool.h> //for bool
#include <stdlib.h> //for malloc
#include "routing.h"
#include "list.h"
#include "msg.h"
#include "main.h"
//#include "list.c"

routingTableNode_t* firstEntry;

stack_t msgQueue;

void initRouting(void)
{
	routingEntry_t mySelf;
	
	mySelf.destinationAddress=node_address;
	mySelf.nextHop=node_address;
	mySelf.hopCount=0;
	
	initRoutingTable(mySelf);
	
	initStack(&msgQueue, 10);
}

void sendFrame(uint8_t *buffer)
{
	int i=0;
	setState(CMD_PLL_ON);
	/* length + CRC */
	TRX_FRAME_BUFFER(0) = buffer[0]+2;
	
	for (i=1;i<buffer[0];i++)
		TRX_FRAME_BUFFER(i) = buffer[i];	

	TRX_STATE = CMD_TX_START;
}

void processRouting(void)
{
	msg_t newMessage;
	newMessage=popStack(&msgQueue);
	//if (newMessage!=OK)
	//	return;
		
		
	if (newMessage.toSend)
		sendFrame(newMessage.buffer);
	
	//sendRouting();
	
	if (newMessage.toRecv);
		//process, if data or if routing message
}


/*void processRouting(void)
{
	msg_t message;
	
	message=pop();
	if (message.toSend)
		sendFrame(message.buffer);
	
	//sendRouting();
	
	if (message.toRecv);
		//process, if data or if routing message
}*/

int formRoutingMessage(uint8_t destination, uint8_t nextHop, uint8_t hopCount, int messageType)
{
	uint8_t *buffer=(uint8_t*)malloc(sizeof(uint8_t)*128);
	buffer[0]=4;
	buffer[1]=messageType;
	buffer[2]=node_address; /* source */
	buffer[3]=destination;
	buffer[4]=hopCount;
	
	push(buffer,toWrite);
	
	//routingEntry_t *temp=(routingEntry_t*) malloc(sizeof(routingEntry_t));
	//temp->destinationAddress=destination;
	//temp->nextHop=nextHop;
	//temp->hopCount=hopCount;
}

int sendRouting(void)
{
	routingTableNode_t *nextEntry;
	nextEntry=firstEntry;
	while (nextEntry!=NULL)
		formRoutingMessage(nextEntry->entry.destinationAddress, nextEntry->entry.nextHop, nextEntry->entry.hopCount, 0xF1);
}


